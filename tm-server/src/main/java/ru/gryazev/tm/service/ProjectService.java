package ru.gryazev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.repository.IProjectRepository;

import java.util.Collections;
import java.util.List;

@Service
public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public List<ProjectEntity> findByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return projectRepository.findByUserId(userId);
    }

    @Nullable
    @Override
    public ProjectEntity findByIdAndUserId(@Nullable String id, @Nullable String userId) {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findByIdAndUserId(id, userId).orElse(null);
    }

    @Nullable
    @Override
    public ProjectEntity findById(@Nullable String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void deleteByIdAndUserId(@Nullable String id, @Nullable String userId) {
        if (id == null || id.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        projectRepository.deleteByIdAndUserId(id, userId);
    }

    @Override
    @Transactional
    public void save(@Nullable ProjectEntity projectEntity) {
        if (projectEntity == null || projectEntity.getUser() == null) return;
        projectRepository.save(projectEntity);
    }

}
