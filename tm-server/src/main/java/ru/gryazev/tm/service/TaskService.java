package ru.gryazev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.repository.ITaskRepository;

import java.util.Collections;
import java.util.List;

@Service
public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public List<TaskEntity> findByProjectId(@Nullable String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findByProjectId(projectId);
    }

    @NotNull
    @Override
    public List<TaskEntity> findByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return taskRepository.findByUserId(userId);
    }

    @Nullable
    @Override
    public TaskEntity findByIdAndUserId(@Nullable String id, @Nullable String userId) {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findByIdAndUserId(id, userId).orElse(null);
    }

    @Override
    @Transactional
    public void save(@Nullable TaskEntity taskEntity) {
        if (taskEntity == null || taskEntity.getUser() == null) return;
        taskRepository.save(taskEntity);
    }

    @Override
    @Transactional
    public void deleteByIdAndUserId(@Nullable String id, @Nullable String userId) {
        if (id == null || id.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        taskRepository.deleteByIdAndUserId(id, userId);
    }

}
