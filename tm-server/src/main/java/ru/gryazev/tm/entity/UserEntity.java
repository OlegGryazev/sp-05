package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.dto.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "app_user")
public class UserEntity extends AbstractCrudEntity {

    private String username;

    private String password;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Role> roles = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<TaskEntity> tasks;

    @Nullable
    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<ProjectEntity> projects;

    @NotNull
    public static User toUserDto(@NotNull final UserEntity userEntity) {
        @NotNull final User user = new User();
        user.setId(userEntity.getId());
        user.setUsername(userEntity.getUsername());
        user.setPassword(userEntity.getPassword());
        return user;
    }

}
