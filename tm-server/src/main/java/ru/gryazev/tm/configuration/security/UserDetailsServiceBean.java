package ru.gryazev.tm.configuration.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.repository.IUserRepository;

import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    private final IUserRepository userRepository;

    @Autowired
    public UserDetailsServiceBean(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final UserEntity userEntity = userRepository.findByUsername(username);
        if (userEntity == null) throw new UsernameNotFoundException("User not found!");
        User.UserBuilder builder = User.withUsername(username);
        builder.password(userEntity.getPassword());
        List<String> roles = new ArrayList<>();
        userEntity.getRoles().forEach(o -> roles.add(o.toString()));
        builder.roles(roles.toArray(new String[] {}));
        return builder.build();
    }

}
