package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.RoleType;

public interface IUserService {

    void createUser(@Nullable String username, @Nullable String password, @Nullable RoleType... roleTypes);

    @Nullable
    UserEntity findByUserName(@Nullable String username);

    @Nullable
    UserEntity findById(@Nullable String id);

    void deleteById(@Nullable String id);

}
