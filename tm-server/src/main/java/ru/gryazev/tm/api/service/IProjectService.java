package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.ProjectEntity;

import java.util.List;

public interface IProjectService {

    @NotNull
    List<ProjectEntity> findByUserId(@Nullable String userId);

    @Nullable
    ProjectEntity findById(@Nullable String id);

    @Nullable
    ProjectEntity findByIdAndUserId(@Nullable String id, @Nullable String userId);

    void deleteByIdAndUserId(@Nullable String id, @Nullable String userId);

    void save(@Nullable ProjectEntity projectEntity);

}
