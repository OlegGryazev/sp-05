package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.TaskEntity;

import java.util.List;

public interface ITaskService {

    @NotNull
    List<TaskEntity> findByProjectId(@Nullable String projectId);

    @NotNull
    List<TaskEntity> findByUserId(@Nullable String userId);

    @Nullable
    TaskEntity findByIdAndUserId(@Nullable String id, @Nullable String userId);

    void save(@Nullable TaskEntity taskEntity);

    void deleteByIdAndUserId(@Nullable String id, @Nullable String userId);

}
