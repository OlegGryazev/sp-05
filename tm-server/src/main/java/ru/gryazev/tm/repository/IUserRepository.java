package ru.gryazev.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import ru.gryazev.tm.entity.UserEntity;

public interface IUserRepository extends CrudRepository<UserEntity, String> {

    @Nullable
    public UserEntity findByUsername(final String username);

}
