<#import "../parts/common.ftl" as c>
<@c.page>
    <div class="col-5">
        <form method="post" action="edit-project">
            <input type="hidden" name="projectId" value="${project.getId()}" />
            <div class="form-group">
                <label for="name">Project name</label>
                <input class="form-control mb-2" type="text" id="name" name="name" value="${project.getName()}" placeholder="Project name" />
            </div>
            <div class="form-group">
                <label for="details">Project details</label>
                <input class="form-control mb-2" type="text" id="details" name="details" value="${project.getDetails()}" placeholder="Project details" />
            </div>
            <div class="form-row">
                <div class="form-group col">
                    <label for="dateStart">Date of project start</label>
                    <#if dateStart??>
                        <input class="form-control mb-2" type="date" id="dateStart" name="dateStart" value="${dateStart}" />
                    <#else>
                        <input class="form-control mb-2" type="date" id="dateStart" name="dateStart" />
                    </#if>
                </div>
                <div class="form-group col">
                    <label for="dateFinish">Date of project finish</label>
                    <#if dateFinish??>
                        <input class="form-control mb-2" type="date" id="dateFinish" name="dateFinish" value="${dateFinish}" />
                    <#else>
                        <input class="form-control mb-2" type="date" id="dateFinish" name="dateFinish" />
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label for="status">Project status</label>
                <select class="form-control" name="status" id="status" >
                    <#list statuses as status>
                        <#if status == project.getStatus()>
                            <option value="${status}" selected>${status.displayName()}</option>
                        <#else>
                            <option value="${status}">${status.displayName()}</option>
                        </#if>
                    </#list>
                </select>
            </div>
            <div class="form-row mt-4">
                <button class="btn btn-primary mr-3" type="submit">Edit project</button>
                <a href="projects" class="btn btn-secondary">Cancel</a>
            </div>
        </form>
    </div>
</@c.page>