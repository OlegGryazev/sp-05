<#include "security.ftl">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Project Manager</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="projects">Projects</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="tasks">Tasks</a>
            </li>
        </ul>

        <#if name == "unknown">
            <form class="form-inline my-2 mr-2 my-lg-0" action="registration" method="get">
                <input class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Registration" />
            </form>
            <form class="form-inline my-2 mr-2 my-lg-0" action="login" method="get">
                <input class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Login" />
            </form>
        <#else>
            <form class="form-inline my-2 mr-2 my-lg-0" action="logout" method="get">
                <label class="mr-3">${name}</label>
                <input class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Logout" />
            </form>
        </#if>
    </div>
</nav>